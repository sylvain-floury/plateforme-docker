<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @Route("/article/create", name="article_creation")
     */
    public function new()
    {
        return $this->render('article/create.html.twig');
    }

    /**
     * @Route("/article/add")
     */
    public function add(Request $request): RedirectResponse
    {
        // TODO à compléter

        return new RedirectResponse('/');
    }

    /**
     * @Route("/article/edit/{id}", name="article_edition")
     * @param $id
     *
     * @return Response
     */
    public function edit(int $id): Response
    {
        // TODO à compléter

        return $this->render('article/edit.html.twig', array(
            'article' => $article
        ));
    }

    /**
     * @Route("/article/update", methods="POST")
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        // TODO à compléter

        return new RedirectResponse('/');
    }

    /**
     * @Route("/article/delete/{id}", requirements={"id" = "\d+"})
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function delete(int $id): RedirectResponse
    {
        // TODO à compléter

        return new RedirectResponse('/');
    }
}