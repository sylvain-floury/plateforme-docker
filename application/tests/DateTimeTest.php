<?php
use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{
    /**
     * @test
     */
    public function shouldFormatDate() {
        $dateTime = new DateTime('2016-09-01');

        $this->assertEquals('01/09/2016', $dateTime->format('d/m/Y')); // <1>
    }

    /**
     * @test
     */
    public function shouldAddInterval() {
        $dateTime = new DateTime('2016-09-01');
        $interval = new DateInterval('P2D'); // <1>
        $dateTime->add($interval);
        $this->assertEquals('03/09/2016', $dateTime->format('d/m/Y'));
    }
}